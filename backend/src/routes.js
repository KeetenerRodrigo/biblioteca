const bookPostgree = require('./app/models/Book');
const express = require('express');
const routes = express.Router();

routes.post('/book', async (req, res) => {
  const { name, writer, publishing } = req.body;

  const createBook = await bookPostgree.create({ name, writer, publishing, like: 0 });

  return res.json({ createBook })
});

routes.get('/book', async (req, res) => {
  const books = await bookPostgree.findAll();
  return res.json({ books })
});

routes.delete('/book/:id', async (req, res) => {
  const { id } = req.params;
  const books = await bookPostgree.destroy({ where: { id: id } })
  return res.json({ books })
});

routes.put('/book/:id/like', async (req, res) => {
  const { like } = req.body;
  const { id } = req.params;

  const books = await bookPostgree.update(
    { like: like += 1 },
    { where: id }
  )

  return res.json({ books })
});

module.exports = routes;