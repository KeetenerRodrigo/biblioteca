module.exports = { 
  up: (queryInterface, Sequelize) => queryInterface.createTable('book', { 
    id: { 
      type: Sequelize.INTEGER, 
      allowNull: false, 
      autoIncrement: true, 
      primaryKey: true, 
    }, 
    name: { 
      type: Sequelize.STRING, 
      allowNull: false, 
      unique: true, 
    }, 
    writer: { 
      type: Sequelize.STRING, 
      allowNull: false,  
    }, 
    publishing: { 
      type: Sequelize.STRING, 
      allowNull: false,  
    }, 
    like: { 
      type: Sequelize.INTEGER, 
      allowNull: false,  
    }, 
    created_at: { 
      type: Sequelize.DATE, 
      allowNull: false, 
    }, 
    updated_at: { 
      type: Sequelize.DATE, 
      allowNull: false, 
    }, 
  }), 
  
  down: (queryInterface) => queryInterface.dropTable('books'), 
}; 