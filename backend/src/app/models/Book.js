const { Model, Sequelize } = require('sequelize');

class Book extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        writer: Sequelize.STRING,
        publishing: Sequelize.STRING,
        like: Sequelize.NUMBER,
      }, {
      sequelize,
    },
    );
  }
}

module.exports = Book;