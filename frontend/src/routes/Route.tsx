import React from 'react';
import { RouteProps as ReactDOMRoutesProps } from 'react-router-dom';

interface RoutesProps extends ReactDOMRoutesProps {
  component: React.ComponentType;
}

const Route: React.FC<RoutesProps> = ({
  component: Component
}) => {
  return (
    <Component />
  );
};

export default Route;
