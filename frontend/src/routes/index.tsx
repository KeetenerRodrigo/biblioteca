import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';

import NewBook from '../pages/Newbook';
import Dashboard from '../pages/Dashboard';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/newbook" component={NewBook} />
    <Route path="/" component={Dashboard} />
  </Switch>
);

export default Routes;
