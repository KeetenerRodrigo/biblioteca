import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.button`
  background: #39A0ED;
  height: 45px;
  border-radius: 4px;
  border: 0;
  padding: 0 16px;
  color: #f9f9f9;
  width: 50%;
  font-weight: 500;
  margin-top: 50px;
  transition: background-color 0.2s;

  display: flex;
  align-items: center;
  justify-content: center;
  margin:20px auto 0;

  &:hover {
    background: ${shade(0.2, '#39A0ED')};
  }
`;
