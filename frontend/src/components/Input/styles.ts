import styled, { css } from 'styled-components';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #fff;
  border-radius: 4px;
  padding: 16px;
  width: 100%;
  border: 1px solid #DDD;
  color: #666360;
  display: flex;
  align-items: center;

  & + div {
    margin-top: 8px;
  }

  ${(props) =>
    props.isErrored &&
    css`
      border-color: #c53030;
    `}

  ${(props) =>
    props.isFocused &&
    css`
      color: #39A0ED;
      border-color: #39A0ED;
    `}

  ${(props) =>
    props.isFilled &&
    css`
      color: #39A0ED;
    `}


  input {
    flex: 1;
    background: transparent;
    border: 0;
    color: #4c4c4c;

    &::placeholder {
      color: #909090;
    }
  }

  svg {
    margin-right: 16px;
  }
`;
