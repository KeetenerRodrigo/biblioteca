import React, { useCallback, useRef } from 'react';
import { FiArrowLeft, FiUser, FiCameraOff, FiBook, FiType } from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import { useHistory, Link } from 'react-router-dom';

import api from '../../services/api';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { Container, Content, AvatarInput } from './styles';

interface BookFormData {
  name: string;
  writer: string;
  publishing: string;
}

const Book: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const history = useHistory();
  
  const handleSubmit = useCallback(
    async (data: BookFormData) => {
        const {
          name,
          writer,
          publishing
        } = data;

        const formData = {
          name,
          writer,
          publishing
        };

        await api.post('/book', formData);
      
        history.push('/');
      },
    [history]
  );

  return (
    <Container>
      <header>
        <div>
          <Link to="/dashboard">
            <FiArrowLeft />
          </Link>
        </div>
      </header>
      <Content>
        <Form
          ref={formRef}
          onSubmit={handleSubmit}
        >
          <AvatarInput>
            <img src={''} alt={''} />
            <label htmlFor="avatar">
              <FiCameraOff />
              <input type="file" id="avatar"/>
            </label>
          </AvatarInput>
          <Input name="name" icon={FiType} placeholder="Título" />
          <Input name="writer" icon={FiUser} placeholder="Escritor" />
          <Input name="publishing" icon={FiBook} placeholder="Editora" />
          <Button type="submit">Salvar Livro</Button>
        </Form>
      </Content>
    </Container>
  );
};

export default Book;
