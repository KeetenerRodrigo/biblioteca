import styled from 'styled-components';

export const Container = styled.div``;

export const Content = styled.main`
  margin: 10px 10px auto;
`;

export const Section = styled.section`
  margin-top: 48px;
  display: flexbox;
  flex-wrap: wrap;
  justify-content:space-between;


  > strong {
    color: #4c4c4c;
    font-size: 20px;
    line-height: 26px;
    border-bottom: 1px solid #3e3b47;
    display: block;
    padding-bottom: 16px;
    margin-bottom: 16px;
  }

  > p {
    color: #4c4c4c;
  }
`;

export const Appointment = styled.div`
  display: flex;
  align-items: center;
  padding: 24px;

  & + div {
    margin-top: 16px;
  }

  div{
    flex: 1;
    background: #fff;
    padding: 16px 24px;
    border-radius: 10px;
    
    border: 2px solid #DDD;
    border-style:solid;
    width:300px;
    height:400px;

    img {
      width: 200px;
      height: 200px;
      display: flex;
      align-items: center;
      margin: 0 auto;
    }

    strong {
      color: #4c4c4c;
      font-size: 16px;
      margin: 12px auto 0; 
      display: flex; 
      align-items: center;
      white-space: pre-line;
    }

    h2 {
      color: #39A0ED;
      margin-left: 28px;
    }

    svg {
      margin-top:30px;
      margin-left:15px;
      color: #39A0ED;
    }
  }
`;
