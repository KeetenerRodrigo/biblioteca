import React, { useEffect, useState } from 'react';
import { FiTrash, FiSmile, FiEdit } from "react-icons/fi";
import { Link } from 'react-router-dom';
import api from '../../services/api';
import Button from '../../components/Button';
import {
  Container,
  Content,
  Section,
  Appointment
} from './styles';

interface Book {
  id: string;
  name: string;
  writer: string;
  publishing: string;
  like: number;
}

const Dashboard: React.FC = () => {
  const [books, setBooks] = useState<Book[]>([]);

  useEffect(() => {
    api
      .get<Book[]>('/book', {
      })
      .then((response) => {
        setBooks(response.data);
      });
  }, []);

  async function handleRemoveBook(id: string) {
    const response = await api.delete(`books/${id}`);

    if (response.status === 204) {
      api
        .get<Book[]>('/book', {
        })
        .then((response) => {
          setBooks(response.data);
        });
    }
  };

  async function handleLikeBook(id: string) {
    await api.post(`book/${id}/like`);

    api
      .get<Book[]>('/book', {
      })
      .then((response) => {
        setBooks(response.data);
      });
  };

  return (
    <Container>
      <Content>

        <Link to="/newbook">
          <Button>
            Adicionar Livro
        </Button>
        </Link>
        <Section>
          {books.map(book => (
            <Appointment key={book.id} style={{ marginTop: 0 }}>
              <div>
                <img />
                <strong><b>Livro:&nbsp;</b>{book.name}</strong>
                <strong><b>Autor:&nbsp;</b>{book.writer}</strong>
                <strong><b>Editora:&nbsp;</b>{book.publishing}</strong>
                <Link to="#" onClick={() => handleLikeBook(book.id)} style={{ fontSize: 25, padding: 20 }}>
                  <FiSmile />
                </Link>
                <Link to="#" style={{ fontSize: 25, padding: 20 }}>
                  <FiEdit />
                </Link>
                <Link to="#" onClick={() => handleRemoveBook(book.id)} style={{ fontSize: 25, padding: 20 }}>
                  <FiTrash />
                </Link>

                <h2>({book.like})</h2>
              </div>
            </Appointment>

          ))}
        </Section>
      </Content>
    </Container>
  );
};

export default Dashboard;
